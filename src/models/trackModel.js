const mongoose = require('mongoose');

const Track = mongoose.model('Track', {
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    type: {
        type: String,
        "enum": ['SPRINTER','SMALL STRAIGHT','LARGE_STRAIGHT'],
        required: true
    },
    status: {
        type: String,
        "enum": ['OL','IS'],
        required: true,
        "default": 'OL'
    },
    created_date: {
        type: Date,
        "default": Date.now()
    }
});

module.exports = { Track };