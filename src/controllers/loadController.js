const express = require('express');
const router = express.Router();
const {
    asyncWrapper
} = require('../utils/apiUtils');

const midllewareFunc =  async (req, res, next) => {
    next();
};

router.get('/', midllewareFunc, asyncWrapper(async (req, res) => {

    res.json({
        "loads": [
            {
                "_id": "5099803df3f4948bd2f98391",
                "created_by": "5099803df3f4948bd2f98391",
                "assigned_to": "5099803df3f4948bd2f98391",
                "status": "NEW",
                "state": "En route to Pick Up",
                "name": "Moving sofa",
                "payload": 100,
                "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
                "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
                "dimensions": {
                    "width": 44,
                    "length": 32,
                    "height": 66
                },
                "logs": [
                    {
                        "message": "Load assigned to driver with id ###",
                        "time": "2020-10-28T08:03:19.814Z"
                    }
                ],
                "created_date": "2020-10-28T08:03:19.814Z"
            }
        ]
    });
}));

router.post('/', midllewareFunc, asyncWrapper(async (req, res) => {
    res.json({
        "name": "Moving sofa",
        "payload": 100,
        "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
        "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
        "dimensions": {
            "width": 44,
            "length": 32,
            "height": 66
        }
    });

}));

router.get('/active', midllewareFunc, asyncWrapper(async (req, res) => {

    const { id } = req.params;

    res.json({
        "load": {
            "_id": "5099803df3f4948bd2f98391",
            "created_by": "5099803df3f4948bd2f98391",
            "assigned_to": "5099803df3f4948bd2f98391",
            "status": "NEW",
            "state": "En route to Pick Up",
            "name": "Moving sofa",
            "payload": 100,
            "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
            "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
            "dimensions": {
                "width": 44,
                "length": 32,
                "height": 66
            },
            "logs": [
                {
                    "message": "Load assigned to driver with id ###",
                    "time": "2020-10-28T08:03:19.814Z"
                }
            ],
            "created_date": "2020-10-28T08:03:19.814Z"
        }
    });
}));

router.patch('/active/state', midllewareFunc, asyncWrapper(async (req, res) => {


    res.json({
        "message": "Load state changed to 'En route to Delivery'"
    });
}));

router.get('/:id', midllewareFunc, asyncWrapper(async (req, res) => {


    res.json({
        "message": "Return loads"
    });
}));

router.put('/:id', midllewareFunc, asyncWrapper(async (req, res) => {


    res.json({
        "message": "Update user's load"
    });
}));

router.delete('/:id', midllewareFunc, asyncWrapper(async (req, res) => {


    res.json({
        "message": "Delete user's load"
    });
}));

router.post('/:id/post', midllewareFunc, asyncWrapper(async (req, res) => {


    res.json({
        "message": "Post user's load"
    });
}));

router.get('/:id/shipping_info', midllewareFunc, asyncWrapper(async (req, res) => {


    res.json({
        "message": "GEt shipping info load"
    });
}));

module.exports = {
    loadRouter: router
}