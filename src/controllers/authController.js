const express = require('express');
const router = express.Router();
const {
    registration,
    signIn
} = require('../services/authService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

const midllewareFunc =  async (req, res, next) => {
    next();
};

router.post('/register', midllewareFunc, asyncWrapper(async (req, res) => {
    const {
        email,
        password,
        role
    } = req.body;

    await registration({email, password, role});

    res.json({
        "message": "Profile created successfully"
    });
}));

router.post('/login', midllewareFunc, asyncWrapper(async (req, res) => {
    const {
        email,
        password
    } = req.body;

    const token = await signIn({email, password});

    res.json({"jwt_token": token});

}));

router.post('/forgot_password', midllewareFunc, asyncWrapper(async (req, res) => {
    const {
        email
    } = req.body;
    // TODO: implement forgot password

    res.json({
        "message": "New password sent to your email address"
    });
}));

module.exports = {
    authRouter: router
}