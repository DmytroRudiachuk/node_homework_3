const express = require('express');
const router = express.Router();
const {
    deleteUserById,
    getUserByEmail
} = require('../services/userService');
const {
    asyncWrapper
} = require('../utils/apiUtils');

const midllewareFunc =  async (req, res, next) => {
    next();
};

router.get('/me', asyncWrapper(async (req, res) => {

    const user = await getUserByEmail(req.user.userId)

    res.json(user)
}));

router.delete('/me', asyncWrapper(async (req, res) => {

    const user = await deleteUserById(req.user.userId)

    res.json({
        "message": "Deleted successfully"
    });

}));

router.patch('/me/password', midllewareFunc, asyncWrapper(async (req, res) => {

    // TODO: implement me

    res.json({
        "message": "Password changed successfully"
    });
}));

module.exports = {
    userRouter: router
}