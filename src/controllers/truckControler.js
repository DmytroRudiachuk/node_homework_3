const express = require('express');
const router = express.Router();
const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    getUserByEmail
} = require('../services/userService');

const {
    createNewTrack,
    getAllTracksByUserId
} = require('../services/trackService');

const midllewareFunc =  async (req, res, next) => {
    next();
};

router.get('/', asyncWrapper(async (req, res) => {

    const user = await getUserByEmail(req.user.userId)
    const track = await getAllTracksByUserId(req.user.userId)

    res.json(track);
}));

router.post('/', asyncWrapper(async (req, res) => {

    const type = req.body.type

    const user = await getUserByEmail(req.user.userId)
    const track = await createNewTrack(req.user.userId, req.user.userId, type)

    res.json({
        "message": "Truck created successfully"
    });

}));

router.get('/:id', midllewareFunc, asyncWrapper(async (req, res) => {

    const { id } = req.params;

    res.json({
        "truck": {
            "_id": "5099803df3f4948bd2f98391",
            "created_by": "5099803df3f4948bd2f98391",
            "assigned_to": "5099803df3f4948bd2f98391",
            "type": "SPRINTER",
            "status": "IS",
            "created_date": "2020-10-28T08:03:19.814Z"
        }
    });
}));

router.put('/:id', midllewareFunc, asyncWrapper(async (req, res) => {


    res.json({
        "message": "Truck details changed successfully"
    });
}));

router.delete('/:id', midllewareFunc, asyncWrapper(async (req, res) => {


    res.json({
        "message": "Truck deleted successfully"
    });
}));

router.post('/:id/assign', midllewareFunc, asyncWrapper(async (req, res) => {


    res.json({
        "message": "Truck assigned successfully"
    });
}));

module.exports = {
    truckRouter: router
}