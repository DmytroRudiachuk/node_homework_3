const express = require('express');
const path = require('path');
const morgan = require('morgan')
const mongoose = require('mongoose')
const app = express();
const port = 8080

const {authRouter} = require('./controllers/authController');
const {loadRouter} = require('./controllers/loadController');
const {truckRouter} = require('./controllers/truckControler');
const {userRouter} = require('./controllers/userController');
const {authMiddleware} = require('./middlewares/authMiddleware');

app.use(express.json())
app.use(morgan('tiny'));

app.use('/api/users', [authMiddleware], userRouter);
app.use('/api/trucks', [authMiddleware], truckRouter);
app.use('/api/loads', loadRouter);
app.use('/api/auth', authRouter);

app.use((req, res, next) => {

    res.status(404).json({message: 'Not found'})
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://test3:test3@cluster0.gxgi7.mongodb.net/test?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });

        app.listen(port);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();



