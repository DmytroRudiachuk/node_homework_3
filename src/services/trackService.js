const {Track} = require('../models/trackModel');

const createNewTrack = async (createdBy, assignedTo, type) => {
    const track = new Track({
        created_by: createdBy,
        assigned_to: assignedTo,
        type: type
    });
    await track.save();
}

const getAllTracksByUserId = async (userId) => {
    const tracks = await Track.find({"created_by" : userId});
    return tracks;
}

module.exports = {
    createNewTrack,
    getAllTracksByUserId
};